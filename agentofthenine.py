import os

import discord
from discord.ext import commands


ext_list = ['exts.welcomer', 'exts.expandochannels', 'exts.miscellaneous']

bot = commands.Bot(command_prefix="]")

if __name__ == '__main__':
    for ext in ext_list:
        bot.load_extension(ext)

bot.run(os.environ.get("DISCORD_BOT_TOKEN"), bot=True, reconnect=True)
