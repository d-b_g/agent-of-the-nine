import json

import discord
from discord.ext import commands

EXPANDO_PREFIX = "🎮 "
EXPANDO_DEFAULT_NAME = "Fireteam Chat"
EXPANDO_DEFAULT_LIMIT = 0


class ExpandoChannels(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_voice_state_update(self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState):
        if before.channel == after.channel:
            return

        if before.channel != None and len(before.channel.members) == 0:
            if before.channel.name.startswith(EXPANDO_PREFIX):
                await before.channel.delete()

        if after.channel != None and len(after.channel.members) == 1:
            if after.channel.name.startswith(EXPANDO_PREFIX):
                await after.channel.guild.create_voice_channel(EXPANDO_PREFIX + EXPANDO_DEFAULT_NAME, category=after.channel.category, user_limit=EXPANDO_DEFAULT_LIMIT)

    @commands.command()
    async def renamevc(self, ctx: commands.Context, *args):
        if ctx.guild != None:
            if ctx.author.voice.channel != None and ctx.author.voice.channel.name.startswith(EXPANDO_PREFIX):
                if len(args) > 0:
                    await ctx.author.voice.channel.edit(name=EXPANDO_PREFIX + " ".join(args))
                    await ctx.send("Your current expando channel has had its name changed to `" + EXPANDO_PREFIX + " ".join(args) + "`!")
                else:
                    await ctx.author.voice.channel.edit(name=EXPANDO_PREFIX + EXPANDO_DEFAULT_NAME)
                    await ctx.send("Invalid name detected, set to the default name for now.")
            else:
                await ctx.send("This command can only be run when within an expando channel, look for a voice channel that starts with `" + EXPANDO_PREFIX + "`!")
        else:
            await ctx.send("This command must be used within the clan bots channel!")
    
    @commands.command()
    async def limitvc(self, ctx: commands.Context, *args):
        if ctx.guild != None:
            if ctx.author.voice.channel != None and ctx.author.voice.channel.name.startswith(EXPANDO_PREFIX):
                if len(args) > 0 and int(args[0]) != None and int(args[0]) >= 0 and int(args[0]) < 100:
                    await ctx.author.voice.channel.edit(user_limit=int(args[0]))
                    await ctx.send("Your current expando channel has been limited to " + str(int(args[0])) + " users!")
                else:
                    await ctx.author.voice.channel.edit(user_limit=EXPANDO_DEFAULT_LIMIT)
                    await ctx.send("Invalid user limit number detected, set to the default user limit for now.")
            else:
                await ctx.send("This command can only be run when within an expando channel, look for a voice channel that starts with `" + EXPANDO_PREFIX + "`!")
        else:
            await ctx.send("This command must be used within the clan bots channel!")

def setup(bot):
    bot.add_cog(ExpandoChannels(bot))
