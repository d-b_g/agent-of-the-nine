import discord
from discord.ext import commands
from discord import channel

PS_CLAN_LINK = "https://www.bungie.net/en/ClanV2?groupId=3734148"
PC_CLAN_LINK = "https://www.bungie.net/en/ClanV2?groupId=3726168"
XBOX_CLAN_LINK = "https://www.bungie.net/en/ClanV2?groupId=3769381"

class MiscellaneousCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def scotxur(self, ctx: commands.Context):
        await ctx.send(file=discord.File("res/mewyllinnitmeow.png"))

    @commands.command()
    async def clanlink(self, ctx: commands.Context, platform: str):
        if platform in ("ps", "ps4", "ps5"):
            link = "Before joining, make ***sure*** that you have read through all the rules in <#657070263535403028>.  When you're done, join here: " + PS_CLAN_LINK
        elif platform in ("pc", "steam"):
            link = "Before joining, make ***sure*** that you have read through all the rules in <#657070263535403028>.  When you're done, join here: " + PC_CLAN_LINK
        elif platform in ("xbox"):
            link = "Before joining, make ***sure*** that you have read through all the rules in <#657070263535403028>.  When you're done, join here: " + XBOX_CLAN_LINK
        else:
            link = "Unknown platform, please put either `ps`, `pc`, or `xbox` after the command (e.g. `]clanlink pc`)"
        await ctx.send(link)

    @clanlink.error
    async def clanlink_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("Please put either `ps`, `pc`, or `xbox` after the command (e.g. `]clanlink pc`)")

def setup(bot):
    bot.add_cog(MiscellaneousCog(bot))