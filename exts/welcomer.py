import json

import discord
from discord.ext import commands

CHARLEMAGNE_ID = 296023718839451649
WELCOME_ID = 532756968151056410
GENERAL_ID = 226205496149934081

def member_has_registered(memb: discord.Member):
    for role in memb.roles:
        if role.id == 558909576121155585:
            return True
    return False

class WelcomerCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        with open('res/welcome.json') as f:
            self.welcome_msg = json.loads(f.read())

    @commands.Cog.listener()
    async def on_message(self, msg: discord.Message):
        # Only care about messages sent in the welcome channel
        if msg.channel.id == WELCOME_ID:

            # Charlemagne successful registration prompt
            if msg.author.id == CHARLEMAGNE_ID and msg.content.endswith(": Successfully synced."):
                await msg.channel.send("Now, just say `!ps`, `!pc`, `!xbox`, or `!stadia` and you're all set.  Head to <#226205496149934081> and introduce yourself!")

            # New member welcome announcement in #general
            if msg.content.split()[0] in ("!ps", "!pc", "!xbox", "!stadia") and member_has_registered(msg.author):
                await msg.guild.get_channel(GENERAL_ID).send(f"{msg.author.mention} has just finished registering!  Please give them a warm welcome!")

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        # New member welcome guide message
        for msg in self.welcome_msg:
            await member.guild.get_channel(WELCOME_ID).send(msg.replace("user", member.mention))


def setup(bot):
    bot.add_cog(WelcomerCog(bot))